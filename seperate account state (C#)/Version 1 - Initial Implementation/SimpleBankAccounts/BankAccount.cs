﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleBankAccounts
{
    public partial class BankAccount
    {
        private AccountState state;

        public decimal OverdraftLimit { get; set; }
        public decimal Balance { get; set;  }

        private NotOverdrawnState notOverdrawn = new NotOverdrawnState();
        private OverdrawnState overdrawn = new OverdrawnState();
        private FrozenState frozen = new FrozenState();
        private ClosedState Closed = new ClosedState();

        protected NotOverdrawnState NotOverdrawn { get { return notOverdrawn; } }
        protected OverdrawnState Overdrawn { get { return overdrawn; } }
        protected FrozenState Frozen { get { return frozen; } }
        protected ClosedState closed { get { return closed; } }

        public BankAccount(decimal amount)
        {
            notOverdrawn.MyAccount = this;
            overdrawn.MyAccount = this;
            frozen.MyAccount = this;
            closed.MyAccount = this;

            Balance = amount;
            state = notOverdrawn;   
        }

        public void Deposit(decimal amount)
        {
            state = state.Deposit(amount);
        }

        protected AccountState State
        {
            get { return state; }
            set
            {
                if (state != null)
                    state.ExitState();

                state = value;

                if (state != null)
                    state.EnterState();
            }
        }

    }
}
