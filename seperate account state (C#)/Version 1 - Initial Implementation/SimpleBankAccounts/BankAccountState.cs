﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleBankAccounts
{
    public partial class BankAccount
    {
        protected abstract class AccountState
        {
            public BankAccount MyAccount { get; set; }
            public virtual AccountState Deposit(decimal amount) { return this; }
            public virtual AccountState Withdrawn(decimal amount) { return this; }
            public virtual AccountState Close(decimal amount) { return this; }

            public virtual void EnterState() {}
            public virtual void ExitState() {}
        }

        protected class OpenState : AccountState
        {
            public override AccountState Deposit(decimal amount)
            {
                AccountState newState = this;
                MyAccount.Balance += amount;
                if (MyAccount.Balance >= 0)
                    newState = MyAccount.NotOverdrawn;
                return newState;
            }

            private void oneDayHandler(object state)
            {
                MyAccount.Balance -= 10;
            }
        }

        protected class NotOverdrawnState : OpenState
        {

        }

        protected class OverdrawnState : OpenState
        {
            private Timer oneDayTimer;

            public int PenaltyPeriod { get; set; }
            public decimal OverdrawnPenalty { get; set; }

            public override void EnterState()
            {
                base.EnterState();
                oneDayTimer = new Timer(OneDayHandler, null, PenaltyPeriod, PenaltyPeriod);
            }

            public override void ExitState()
            {
                oneDayTimer.Dispose();
                oneDayTimer = null;
                base.ExitState();
            }

            private void OneDayHandler(object state)
            {
                MyAccount.Balance -= OverdrawnPenalty;
            }
        }

        protected class FrozenState : AccountState
        {
        }

        protected class ClosedState : AccountState
        {
        }

    }
}
