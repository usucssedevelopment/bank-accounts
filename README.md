# bank-accounts

This repository contains variations of a  bank-account class library to illustrate good and bad abstraction, modularization, and encapsulation. Below is a list of sub-directories and their intended content:

* accounts only
  * good abstraction
  * poor abstraction - bad names
  * poor abstraction - missing and redundant elements
* customers and accounts only
  * good abstraction
  * poor abstraction - bad names
  * poor abstraction - missing and redundant elements